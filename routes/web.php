<?php

use App\Models\Post;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('jobs', function () {
    // @TODO: Добавить получение списка задач в очереди
    return view('jobs', ['jobs' => [['id' => 1, 'name' => 'Test'], ['id' => 2, 'name' => 'Test 2']]]);
});

Route::get('job-start', function () {
    // Запуск задачи на загрузку курсов валют
    dispatch(new \App\Jobs\LoadCbrDailyJob());

    return view('job-start');
});

Route::get('posts', function () {
    return view('posts', [
        'posts' => Post::all(),
    ]);
});

Route::get('posts/{post}', function ($slug) {
    return view('post', [
        'post' => Post::findOrFail($slug)
    ]);
});
