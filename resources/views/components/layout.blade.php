<!DOCTYPE html>
<html>
<head>
    <title>My Blog</title>
    <link rel="stylesheet" href="/assets/css/app.css">
</head>
<body>
    <main id="app">
        {{ $slot }}
    </main>

    <script src="/js/app.js"></script>
</body>
</html>
