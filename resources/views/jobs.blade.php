<x-layout>
    <h2>Список задач</h2>

    <example-component :items="{{ json_encode($jobs) }}"></example-component>
</x-layout>
