<x-layout>
    <article>
        <h1>{{ $post->title }}</h1>
        <div>
            {!! $post->body !!}
        </div>
    </article>
    <sidebar><a href="/posts/">Go back</a></sidebar>
</x-layout>
