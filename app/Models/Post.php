<?php

namespace App\Models;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\File;
use Spatie\YamlFrontMatter\YamlFrontMatter;

class Post
{
    public $title;

    public $desc;

    public $body;

    public $date;

    public $slug;

    /**
     * @param $title
     * @param $desc
     * @param $body
     * @param $date
     * @param $slug
     */
    public function __construct($title, $desc, $body, $date, $slug)
    {
        $this->title = $title;
        $this->desc = $desc;
        $this->body = $body;
        $this->date = $date;
        $this->slug = $slug;
    }

    public static function all(): \Illuminate\Support\Collection
    {
        return cache()->rememberForever('posts.all', function () {
            return collect(File::files(resource_path("posts/")))
                ->map(fn($file) => YamlFrontMatter::parseFile($file))
                ->map(fn($document) => new Post(
                    $document->title,
                    $document->desc,
                    $document->body(),
                    $document->date,
                    $document->slug
                ))
                ->sortByDesc('date');
        });
    }

    public static function find($slug)
    {
        return static::all()->firstWhere('slug', $slug);
    }

    public static function findOrFail($slug)
    {
        return static::find($slug) ?: throw new ModelNotFoundException();
    }
}
