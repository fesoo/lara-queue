<?php

namespace App\Http\Controllers;

use App\Models\Currency as CurrencyModel;
use Illuminate\Support\Facades\Http;

class Currency extends Controller
{
    protected static string $apiUrl = 'https://www.cbr.ru/scripts/XML_daily.asp?date_req=';

    // Импортируем курсы валют
    public static function import(\DateTime $date = null)
    {
        if (!$date) {
            $date = new \DateTime();
        }

        $response = Http::get(self::$apiUrl . $date->format('d/m/Y'));
        $currencyList = \simplexml_load_string($response->body());
        if ($currencyList) {
            $date = (string) $currencyList->attributes()->{'Date'};

            foreach ($currencyList->Valute as $valute) {
                // Сохраняем курс валюты в БД
                $currencyItem = CurrencyModel::where([
                    'code' => (string) $valute->attributes()->{'ID'},
                    'date' => (new \DateTime($date))->format('Y-m-d'),
                ])->get();
                if ($currencyItem->isEmpty()) {
                    $currency = new CurrencyModel();
                    $currency->date = (new \DateTime($date));
                    $currency->code = (string) $valute->attributes()->{'ID'};
                    $currency->num = (string) $valute->NumCode;
                    $currency->char = (string) $valute->CharCode;
                    $currency->nominal = (int)$valute->Nominal;
                    $currency->name = (string) $valute->Name;
                    $currency->value = (float)$valute->Value;
                    $res = $currency->save();
                }
            }
        }

        return true;
    }
}
